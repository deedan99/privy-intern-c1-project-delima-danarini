import React from "react";
import ReactDOM from "react-dom";

import App from "./"

ReactDom.render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>,
    document.getElementById("root")
);